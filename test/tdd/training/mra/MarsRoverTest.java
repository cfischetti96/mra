package tdd.training.mra;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;



public class MarsRoverTest {

	
	@Test
	//Test user story 1 obstacle at
	public void test() throws Exception {
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("(4,7)");
		planetObstacles.add("(2,3)");
		MarsRover rover = new MarsRover(10, 10, planetObstacles);
		
		assertTrue(rover.planetContainsObstacleAt(4,7));
		assertTrue(rover.planetContainsObstacleAt(2,3));
	}
	
	@Test
	//Test user story 2
	public void testCommand() throws Exception {
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("(4,7)");
		planetObstacles.add("(2,3)");
		MarsRover rover = new MarsRover(10, 10, planetObstacles);
		
		assertEquals("(0,0,N)",rover.executeCommand(""));
		
	}
	
	@Test
	//Test user story 3 command r
	public void testCommandR() throws Exception {
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("(4,7)");
		planetObstacles.add("(2,3)");
		MarsRover rover = new MarsRover(10, 10, planetObstacles);
		
		assertEquals("(0,0,E)",rover.executeCommand("r"));
		
	}
	
	@Test
	//Test user story 3 command l
	public void testCommandL() throws Exception {
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("(4,7)");
		planetObstacles.add("(2,3)");
		MarsRover rover = new MarsRover(10, 10, planetObstacles);
		
		assertEquals("(0,0,W)",rover.executeCommand("l"));
	}
		
	@Test
	//Test user story 4 command f
	public void testCommandF() throws Exception {
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("(4,7)");
		planetObstacles.add("(2,3)");
		MarsRover rover = new MarsRover(10, 10, planetObstacles);
		rover.setPosition(7,7,'N');
		
		assertEquals("(7,8,N)",rover.executeCommand("f"));
	}
	
	@Test
	//Test user story 5 command b
	public void testCommandB() throws Exception {
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("(4,7)");
		planetObstacles.add("(2,3)");
		MarsRover rover = new MarsRover(10, 10, planetObstacles);
		rover.setPosition(7,7,'N');
		
		assertEquals("(7,6,N)",rover.executeCommand("b"));
	}
	
	@Test
	//Test user story 6 
	public void testCommandCombined() throws Exception {
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("(4,7)");
		planetObstacles.add("(2,3)");
		MarsRover rover = new MarsRover(10, 10, planetObstacles);
		
		assertEquals("(2,2,E)",rover.executeCommand("ffrff"));
	}
	
	@Test
	//Test user story 7
	public void testCommandWrapping() throws Exception {
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("(4,7)");
		planetObstacles.add("(2,3)");
		MarsRover rover = new MarsRover(10, 10, planetObstacles);
		
		assertEquals("(0,8,N)",rover.executeCommand("bb"));
	}
	
	@Test
	//Test user story 8 single obstacle
	public void testCommandObstacle() throws Exception {
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("(2,2)");
		MarsRover rover = new MarsRover(10, 10, planetObstacles);
		
		assertEquals("(1,2,E)(2,2)",rover.executeCommand("ffrfff"));
	}
	
	@Test
	//Test user story 9 multiple obstacles
	public void testMultipleObstacles() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("(2,2)");
		planetObstacles.add("(2,1)");
		MarsRover rover = new MarsRover(10, 10, planetObstacles);
		assertEquals("(1,1,E)(2,2)(2,1)", rover.executeCommand("ffrfffrflf"));
	}
	
	@Test
	//Test user story 10 wrapping and obstacles
	public void testMultipleWrapAndOb() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("(0,9)");
		MarsRover rover = new MarsRover(10, 10, planetObstacles);
		assertEquals("(0,0,N)(0,9)", rover.executeCommand("b"));
	}
	
	@Test
	//Test user story 11
	public void testTour() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("(2,2)");
		planetObstacles.add("(0,5)");
		planetObstacles.add("(5,0)");
		MarsRover rover = new MarsRover(6, 6, planetObstacles);
		assertEquals("(0,0,N)(2,2)(0,5)(5,0)", rover.executeCommand("ffrfffrbbblllfrfrbbl"));
	}

}
