package tdd.training.mra;

import java.util.List;




public class MarsRover {
	
	protected List<String> planetObstacles;
	protected int planetX;
	protected int planetY;
	protected int posX;
	protected int posY;
	protected char dir;
	protected String obstacle = "";
	

	/**
	 * It initializes the rover at the coordinates (0,0), facing North, on a planet
	 * (represented as a grid with x and y coordinates) containing obstacles.
	 * 
	 * @param planetX         The x dimension of the planet.
	 * @param planetY         The y dimension of the planet.
	 * @param planetObstacles The obstacles on the planet. Each obstacle is a string
	 *                        (without white spaces) formatted as follows:
	 *                        "(oi_x,oi_y)". <code>null</code> if the planet does
	 *                        not contain obstacles.
	 * 
	 * @throws MarsRoverException
	 */
	public MarsRover(int planetX, int planetY, List<String> planetObstacles) throws MarsRoverException {
		this.planetX = planetX;
		this.planetY = planetY;
		this.planetObstacles = planetObstacles;
		this.posX = 0;
		this.posY = 0;
		this.dir = 'N';
	}

	/**
	 * It returns whether, or not, the planet (where the rover moves) contains an
	 * obstacle in a cell.
	 * 
	 * @param x The x coordinate of the cell
	 * @param y The y coordinate of the cell
	 * @return <true> if the cell contains an obstacle, <false> otherwise.
	 * @throws MarsRoverException
	 */
	public boolean planetContainsObstacleAt(int x, int y) throws MarsRoverException {
		int a;
		int b;
		if(x > this.planetX-1) a = 0;
		else if(x < 0) a = this.planetX-1;
		else a = x;
		
		if(y > this.planetY-1) b = 0;
		else if(y < 0) b = this.planetY-1;
		else b = y;
		return this.planetObstacles.contains("("+a+","+b+")");
	}

	/**
	 * It lets the rover move on the planet according to a command string. The
	 * return string contains the new position of the rover, its direction, and the
	 * obstacles it has encountered while moving on the planet (if any).
	 * 
	 * @param commandString A string that can contain a single command -- i.e. "f"
	 *                      (forward), "b" (backward), "l" (left), or "r" (right) --
	 *                      or a combination of single commands.
	 * @return The return string that contains the position and direction of the
	 *         rover, and the obstacles the rover has encountered while moving on
	 *         the planet (if any). The return string (without white spaces) has the
	 *         following format: "(x,y,dir)(o1_x,o1_y)(o2_x,o2_y)...(on_x,on_y)". x
	 *         and y define the new position of the rover while dir represents its
	 *         direction (i.e., N, S, W, or E). Finally, oi_x and oi_y are the
	 *         coordinates of the i-th encountered obstacle.
	 * @throws MarsRoverException
	 */
	public String executeCommand(String commandString) throws MarsRoverException {
		if(commandString.equals("")) {
			return this.getPosition();
		}
		for (int i = 0; i < commandString.length(); i++){
		    char c = commandString.charAt(i); 
		    
		    if(c == 'r') {
		    	switch(this.dir) {
		    	case('N'):
		    		this.setPosition(posX, posY, 'E');
					break;
		    	case ('E'): 
		    		this.setPosition(posX, posY, 'S');
    				break;
		    	case ('S'): 
		    		this.setPosition(posX, posY, 'W');
    				break;
		    	case ('W'): 
		    		this.setPosition(posX, posY, 'N');
    				break;
		    	default: throw new MarsRoverException("Errore nella posizione");
		    	}
		
		}
		else if(c == 'l') {
				switch(this.dir) {
				case('N'):
					this.setPosition(posX, posY, 'W');
					break;
				case ('E'): 
					this.setPosition(posX, posY, 'N');
    				break;
				case ('S'): 
					this.setPosition(posX, posY, 'E');
    				break;
				case ('W'): 
					this.setPosition(posX, posY, 'S');
    				break;
				default: throw new MarsRoverException("Errore nella posizione");
				}
			
		}

		else if(c == 'f') {
			switch(this.dir) {
			case('N'):
				if(this.posY+1>this.planetY-1) {
					if(!planetContainsObstacleAt(this.posX, 0))
						this.setPosition(posX, 0, this.dir);
					else if(!obstacle.contains("("+this.posX+","+0+")")) obstacle += "("+this.posX+","+0+")";
				}
				else if(!planetContainsObstacleAt(this.posX, this.posY+1))
					this.setPosition(posX, posY+1, this.dir);
				else if(!obstacle.contains("("+this.posX+","+(this.posY+1)+")")) obstacle += "("+this.posX+","+(this.posY+1)+")";
			break;
			case ('E'): 
				if(this.posX+1>this.planetX-1) {
					if(!planetContainsObstacleAt(0, this.posY))
						this.setPosition(0, posY, this.dir);
					else if(!obstacle.contains("("+0+","+this.posY+")")) obstacle += "("+0+","+this.posY+")";
				}
				else if(!planetContainsObstacleAt(this.posX+1, this.posY))
					this.setPosition(posX+1, posY, this.dir);
				else if(!obstacle.contains("("+(this.posX+1)+","+this.posY+")")) obstacle += "("+(this.posX+1)+","+this.posY+")";
				
				 break;
			case ('S'): 
				if(this.posY-1 < 0) {
					if(!planetContainsObstacleAt(this.posX, this.planetY-1))
						this.setPosition(posX, this.planetY-1, this.dir);
					else if(!obstacle.contains("("+this.posX+","+(this.planetY-1)+")")) obstacle += "("+this.posX+","+(this.planetY-1)+")";
				}
				else if(!planetContainsObstacleAt(this.posX, this.posY-1))
					this.setPosition(posX, posY-1, this.dir);
				else if(!obstacle.contains("("+this.posX+","+(this.posY-1)+")")) obstacle += "("+this.posX+","+(this.posY-1)+")";
				
					break;
			case ('W'): 
				if(this.posX-1 < 0) {
					if(!planetContainsObstacleAt(this.planetX-1, this.posY))
						this.setPosition(this.planetX-1, this.posY, this.dir);
					else if(!obstacle.contains("("+(this.planetX-1)+","+(this.posY)+")")) obstacle += "("+(this.planetX-1)+","+(this.posY)+")";
				}
				else if(!planetContainsObstacleAt(this.posX-1, this.posY))
					this.setPosition(posX-1, posY, this.dir);
				else if(!obstacle.contains("("+(this.posX-1)+","+this.posY+")")) obstacle += "("+(this.posX-1)+","+this.posY+")";
				
					break;
			default: throw new MarsRoverException("Errore nella posizione");
		}
	
	}
		else if(c == 'b') {
			switch(this.dir) {
			case('N'):
				if(this.posY-1 < 0) {
					if(!planetContainsObstacleAt(this.posX, this.planetY-1))
						this.setPosition(posX, this.planetY-1, this.dir);
					else if(!obstacle.contains("("+this.posX+","+(this.planetY-1)+")")) obstacle += "("+this.posX+","+(this.planetY-1)+")";
				}
				else if(!planetContainsObstacleAt(this.posX, this.posY-1))
					this.setPosition(posX, posY-1, this.dir);
				else { 
							
					if(!obstacle.contains("("+this.posX+","+(this.posY-1)+")")) obstacle += "("+this.posX+","+(this.posY-1)+")";
				}
					break;
			case ('E'): 
				if(this.posX-1 < 0) {
					if(!planetContainsObstacleAt(this.planetX-1, this.posY))
						this.setPosition(this.planetX-1, this.posY, this.dir);
					else if(!obstacle.contains("("+(this.planetX-1)+","+(this.posY)+")")) obstacle += "("+(this.planetX-1)+","+(this.posY)+")";
				}
				else if(!planetContainsObstacleAt(this.posX-1, this.posY))
					this.setPosition(posX-1, posY, this.dir);
				else if(!obstacle.contains("("+(this.posX-1)+","+this.posY+")")) obstacle += "("+(this.posX-1)+","+this.posY+")";
			
				break;
			case ('S'):
				if(this.posY+1>this.planetY-1) {
					if(!planetContainsObstacleAt(this.posX, 0))
						this.setPosition(posX, 0, this.dir);
					else if(!obstacle.contains("("+this.posX+","+0+")")) obstacle += "("+this.posX+","+0+")";
				}
				else if(!planetContainsObstacleAt(this.posX, this.posY+1))
					this.setPosition(posX, posY+1, this.dir);
									
				else if(!obstacle.contains("("+this.posX+","+(this.posY+1)+")")) obstacle += "("+this.posX+","+(this.posY+1)+")";
				
					break;
			case ('W'):
				
				if(this.posX+1>this.planetX-1) {
					if(!planetContainsObstacleAt(0, this.posY))
						this.setPosition(0, posY, this.dir);
					else if(!obstacle.contains("("+0+","+this.posY+")")) obstacle += "("+0+","+this.posY+")";
				}
				else if(!planetContainsObstacleAt(this.posX+1, this.posY))
					this.setPosition(posX+1, posY, this.dir);

				else if(!obstacle.contains("("+(this.posX+1)+","+this.posY+")")) obstacle += "("+(this.posX+1)+","+this.posY+")";
				
					break;
			default: throw new MarsRoverException("Errore nella posizione");
			}
		
		}
	}
		 return this.getPosition()+obstacle;
	}
	
	

	public String getPosition() {
		return "("+this.posX+","+this.posY+","+dir+")";
	}
	
	
	public void setPosition(int i, int j, char c) {
		if(i > this.planetX-1) this.posX = 0;
		else if(i < 0) this.posX = this.planetX-1;
		else this.posX = i;
		
		if(j > this.planetY-1) this.posY = 0;
		else if(j < 0) this.posY = this.planetY-1;
		else this.posY = j;
		
		this.dir = c;
	}
	
}
